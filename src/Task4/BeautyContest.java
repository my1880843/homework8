package Task4;

import Task4.Models.Dog;
import Task4.Models.Participant;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class BeautyContest {
    private final ArrayList<Participant> participants;

    public BeautyContest() {
        participants = new ArrayList<>();
        setData();
    }

    public void setData() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            participants.add(new Participant(scanner.next()));
        }
        for (int i = 0; i < n; i++) {
            participants.get(i).setDog(new Dog(scanner.next()));
        }
        for (int i = 0; i < n; i++) {
            double average = Math.floor(getAveragePoint(scanner) * 10.0) / 10.0;
            participants.get(i).setAveragePoint(average);
        }
    }

    public void printPrizers() {
        if (participants == null) {
            System.out.println("Сначала заполните участников");
            return;
        }

        participants.sort(Comparator.comparing(Participant::getAveragePoint));

        for (int i = 0; i < 3; i++) {
            var participant = participants.get(i);
            System.out.printf(
                    "%s: %s, %s%n",
                    participant.getName(),
                    participant.getDog().getName(),
                    participant.getAveragePoint()
            );
        }
    }

    private double getAveragePoint(Scanner scanner) {
        double sum = 0;
        for (int j = 0; j < 3; j++) {
            sum += scanner.nextDouble();
        }
        return sum / 3;
    }
}
