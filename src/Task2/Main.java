package Task2;

import Task2.Furnitures.Closet;
import Task2.Furnitures.Stool;
import Task2.Furnitures.Table;

public class Main {
    public static void main(String[] args) {
        Tests.isRepariableTest();
    }

    private static class Tests {
        public static void isRepariableTest() {
            var carpenter = new BestCarpenterEver();

            var table = new Table();
            var closet = new Closet();
            var stool = new Stool();

            if (carpenter.isRepairable(table)) {
                System.out.println("Test isRepariableTest не пройден");
                return;
            }
            if (carpenter.isRepairable(closet)) {
                System.out.println("Test isRepariableTest не пройден");
                return;
            }
            if (!carpenter.isRepairable(stool)) {
                System.out.println("Test isRepariableTest не пройден");
                return;
            }

            System.out.println("Test isRepariableTest пройден");
        }
    }
}


