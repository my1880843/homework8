package Task1.Animals;

import Task1.Enums.MoveType;
import Task1.Enums.SpeedType;
import Task1.Enums.WayOfBirth;
import Task1.Interfaces.IMove;

public abstract class Animal implements IMove {
    private String name;
    private final MoveType moveType;
    private final SpeedType speedType;
    private final WayOfBirth wayOfBirth;

    protected Animal(String name, WayOfBirth wayOfBirth, MoveType moveType, SpeedType speedType) {
        this.name = name;
        this.wayOfBirth = wayOfBirth;
        this.moveType = moveType;
        this.speedType = speedType;
        System.out.printf("Животное %s родилось%n", this.name);
    }

    public String getName() {
        return name;
    }

    public WayOfBirth getWayOfBirth() {
        return this.wayOfBirth;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public SpeedType getSpeedType() {
        return speedType;
    }

    public void sleep() {
        System.out.println(this.name + " спит");
    }

    public void eat() {
        System.out.println(this.name + " ест");
    }

    public void move() {
        System.out.printf("Животное %s %s %s%n", this.name, this.moveType, this.speedType);
    }
}
