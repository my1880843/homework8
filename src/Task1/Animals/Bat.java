package Task1.Animals;

import Task1.Enums.AnimalType;
import Task1.Enums.MoveType;
import Task1.Enums.SpeedType;
import Task1.Enums.WayOfBirth;

public class Bat extends Animal {
    public Bat() {
        super(AnimalType.BAT.toString(), WayOfBirth.Mammal, MoveType.Flying, SpeedType.Slow);
    }
}
