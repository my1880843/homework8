package Task1.Animals;

import Task1.Enums.AnimalType;
import Task1.Enums.MoveType;
import Task1.Enums.SpeedType;
import Task1.Enums.WayOfBirth;

public class GoldFish extends Animal {
    public GoldFish() {
        super(AnimalType.GOLDFISH.toString(), WayOfBirth.Fish, MoveType.Swimming, SpeedType.Slow);
    }
}
