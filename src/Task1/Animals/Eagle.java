package Task1.Animals;

import Task1.Enums.AnimalType;
import Task1.Enums.MoveType;
import Task1.Enums.SpeedType;
import Task1.Enums.WayOfBirth;

public class Eagle extends Animal {
    public Eagle() {
        super(AnimalType.EAGLE.toString(), WayOfBirth.Bird, MoveType.Flying, SpeedType.Fast);
    }
}
