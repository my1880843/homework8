package Task1.Animals;

import Task1.Enums.AnimalType;
import Task1.Enums.MoveType;
import Task1.Enums.SpeedType;
import Task1.Enums.WayOfBirth;

public class Dolphin extends Animal {
    public Dolphin() {
        super(AnimalType.DOLPHIN.toString(), WayOfBirth.Mammal, MoveType.Swimming, SpeedType.Fast);
    }
}
