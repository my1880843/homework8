package Task1;

import Task1.Animals.*;
import Task1.Enums.AnimalType;

public class AnimalFactory {
    public Animal createAnimal(AnimalType animalType) {

        switch (animalType) {
            case BAT -> {
                return new Bat();
            }
            case DOLPHIN -> {
                return new Dolphin();
            }
            case EAGLE -> {
                return new Eagle();
            }
            case GOLDFISH -> {
                return new GoldFish();
            }
            default -> {
                return null;
            }
        }
    }
}
