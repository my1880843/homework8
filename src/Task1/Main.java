package Task1;

import Task1.Enums.AnimalType;

public class Main {
    public static void main(String[] args) {
        var factory = new AnimalFactory();

        var bat = factory.createAnimal(AnimalType.BAT);
        bat.eat();
        bat.move();

        var dolphin = factory.createAnimal(AnimalType.DOLPHIN);
        dolphin.move();
        dolphin.eat();
        dolphin.sleep();
    }
}
