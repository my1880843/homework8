package Task1.Enums;

public enum AnimalType {
    BAT("летучая мышь"),
    DOLPHIN("дельфин"),
    EAGLE("орел"),
    GOLDFISH("золотая рыбка");

    private final String description;

    AnimalType(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
