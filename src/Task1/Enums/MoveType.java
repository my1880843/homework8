package Task1.Enums;

public enum MoveType {
    Flying("летит"),
    Swimming("плывет");
    private final String description;

    MoveType(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
