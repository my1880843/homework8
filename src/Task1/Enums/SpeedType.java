package Task1.Enums;

public enum SpeedType {
    Slow("медленно"),
    Fast("быстро");
    private final String description;

    SpeedType(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
