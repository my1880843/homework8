package Task1.Enums;

public enum WayOfBirth {
    Mammal("Живородящие"),
    Fish("Мечет икру"),
    Bird("Откладывает яйца");

    private final String description;

    WayOfBirth(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
